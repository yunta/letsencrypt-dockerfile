FROM localhost/base
RUN dnf -y install dehydrated python2-lexicon.noarch
RUN pip3 install 'dns-lexicon[henet]'
RUN mkdir /data
WORKDIR /data
ENTRYPOINT ["/bin/bash"]
CMD ["/data/refresh.sh"]
